package com.service.investmentmanager.exception;

public class ListIsEmptyException extends RuntimeException {

    private final String sortBy;

    public ListIsEmptyException(String sortBy) {
        this.sortBy =sortBy;
    }
}
