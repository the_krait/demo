package com.service.investmentmanager.exception;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@Configuration
@PropertySource("classpath:exception.properties")
@ControllerAdvice
public class CustomizedExceptionHandler {
	
	ExceptionResponse exceptionResponse = new ExceptionResponse();

	@Value("${accountIdNotExistEx}")
	private String accountIdNotExistEx;
	
	@ExceptionHandler(AccountIdNotExistException.class)
	public ResponseEntity<Object> handleExceptions( AccountIdNotExistException exception, WebRequest webRequest) {
		exceptionResponse.setDateTime(LocalDateTime.now());
        exceptionResponse.setMessage(accountIdNotExistEx+exception.getAccountId());
        return new ResponseEntity<>(exceptionResponse,HttpStatus.BAD_REQUEST);    
    }

	@Value("${listIsEmptyEx}")
	private String listIsEmptyEx;

	@ExceptionHandler(ListIsEmptyException.class)
	public ResponseEntity<Object> handleExceptions(ListIsEmptyException exception, WebRequest webRequest) {
		exceptionResponse.setDateTime(LocalDateTime.now());
		exceptionResponse.setMessage(listIsEmptyEx);
		return new ResponseEntity<>(exceptionResponse,HttpStatus.BAD_REQUEST);
	}
}
