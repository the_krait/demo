package com.service.investmentmanager.exception;

import lombok.Getter;

@Getter
public class AccountIdNotExistException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long accountId = 0;
	public AccountIdNotExistException(long accountId) {
		super();
		this.accountId = accountId;
	}
}
