package com.service.investmentmanager.controller;

import com.service.investmentmanager.dto.OrderHistoryDto;
import com.service.investmentmanager.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.service.investmentmanager.service.OrderHistoryService;

import java.util.List;

@RestController
@RequestMapping("/api")
public class OrderHistoryController {
	
	@Autowired
	OrderHistoryService orderHistoryInterface;
	
	@GetMapping("/order/history/{accountId}/sort/{sortBy}")
	public ResponseEntity<List<OrderHistoryDto>> getOrderHistoryDetailsInSortedOrder(@PathVariable(required=true) long accountId,@PathVariable String sortBy) {
		return new ResponseEntity<>(orderHistoryInterface.getOrderHistoryDetails(accountId,sortBy), HttpStatus.OK);
	}

	@GetMapping("/order/history/{accountId}/contain/{contain}")
	public ResponseEntity<List<OrderHistoryDto>> getOrderHistoryDetailsContainsOrder(@PathVariable(required=true) long accountId,@PathVariable String contain) {
		return new ResponseEntity<>(orderHistoryInterface.getOrderHistoryDetailsContain(accountId,contain), HttpStatus.OK);
	}
}
