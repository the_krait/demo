package com.service.investmentmanager.service;

import com.service.investmentmanager.dto.OrderHistoryDto;
import com.service.investmentmanager.model.Order;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface OrderHistoryService {

	List<OrderHistoryDto> getOrderHistoryDetails(long accountId, String sortBy);

	List<OrderHistoryDto> getOrderHistoryDetailsContain(long accountId, String contain);
}
