package com.service.investmentmanager.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderHistoryDto {
	
	private String stockName;
	private long orderId;
	private double purchasePrice;
}
