package com.service.investmentmanager.serviceimpl;

import com.service.investmentmanager.dto.OrderHistoryDto;
import com.service.investmentmanager.exception.ListIsEmptyException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import com.service.investmentmanager.exception.AccountIdNotExistException;
import com.service.investmentmanager.model.Order;
import com.service.investmentmanager.repository.OrderHistoryRepository;
import com.service.investmentmanager.service.OrderHistoryService;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;


@Component
public class OrderHistoryServiceImpl implements OrderHistoryService {

	@Autowired
	OrderHistoryRepository orderHistoryRepository;

	ModelMapper modelMapper = new ModelMapper();

	@Override
	public List<OrderHistoryDto> getOrderHistoryDetails(long accountId, String sortBy) {
		boolean result = isAccountIdExist(accountId);
		if(!result) {
			throw new AccountIdNotExistException(accountId);
		}
		List<OrderHistoryDto> dtoList = new ArrayList<>();
		Pageable paging = PageRequest.of(0,10, Sort.by(sortBy).ascending());
		Page<Order> orderList= orderHistoryRepository.findAllByAccountId(accountId,paging);
		if(orderList.isEmpty() || orderList == null) {
			throw new ListIsEmptyException(sortBy);
		}
		orderList.stream().forEach((c) -> {
			OrderHistoryDto orderHistoryDto = modelMapper.map(c,OrderHistoryDto.class);
			dtoList.add(orderHistoryDto);
		});
		return dtoList;
	}

	private boolean isAccountIdExist(long accountId) {
		return ObjectUtils.isEmpty(orderHistoryRepository.findByAccountId(accountId)) ? false : true;
	}

	@Override
	public List<OrderHistoryDto> getOrderHistoryDetailsContain(long accountId, String contain) {
		boolean result = isAccountIdExist(accountId);
		if(!result) {
			throw new AccountIdNotExistException(accountId);
		}
		List<OrderHistoryDto> dtoList = new ArrayList<>();
		List<Order> orderList= orderHistoryRepository.findAllByAccountIdAndStockNameLike(accountId, "%"+contain+"%");
		if(orderList.isEmpty() || orderList == null) {
			throw new ListIsEmptyException(contain);
		}
		orderList.stream().forEach((c) -> {
			OrderHistoryDto orderHistoryDto = modelMapper.map(c,OrderHistoryDto.class);
			dtoList.add(orderHistoryDto);
		});
		return dtoList;
	}
}
