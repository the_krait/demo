package com.service.investmentmanager.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import com.service.investmentmanager.model.Order;

import java.util.List;


public interface OrderHistoryRepository extends PagingAndSortingRepository<Order, Integer> {

    List<Order> findByStockNameLike(String contain);

    List<Order> findByAccountId(long accountId);

	Page<Order> findAllByAccountId(long accountId, Pageable paging);

	List<Order> findAllByAccountIdAndStockNameLike(long accountId, String string);
}
