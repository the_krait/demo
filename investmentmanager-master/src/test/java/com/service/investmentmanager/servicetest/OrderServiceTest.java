package com.service.investmentmanager.servicetest;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;

import com.service.investmentmanager.dto.OrderHistoryDto;
import com.service.investmentmanager.exception.AccountIdNotExistException;
import com.service.investmentmanager.exception.ListIsEmptyException;
import com.service.investmentmanager.repository.OrderHistoryRepository;
import com.service.investmentmanager.serviceimpl.OrderHistoryServiceImpl;

@ExtendWith(MockitoExtension.class)
class OrderServiceTest {
	
	@InjectMocks
	OrderHistoryServiceImpl orderHistoryServiceImpl;
	
	@Mock
	OrderHistoryRepository orderHistoryRepository;
	
	@Spy
	ModelMapper modelMapper = new ModelMapper();
	
	long accountId = 6545;
	String sortBy = "stockName";
	static List<OrderHistoryDto> dtoList = new ArrayList<>();
	static OrderHistoryDto orderHistoryDto;
	static Pageable pageable;
	@BeforeAll
	static void setValues() {
		orderHistoryDto = new OrderHistoryDto();
		orderHistoryDto.setOrderId(1);
		orderHistoryDto.setPurchasePrice(34.23);
		orderHistoryDto.setStockName("hitachi");
		dtoList.add(orderHistoryDto);
	}

	@Test
	void testForOrderHistoryDetails() {
		Mockito.when(orderHistoryServiceImpl.getOrderHistoryDetails(accountId, sortBy)).thenReturn(dtoList);
		assertNotNull(dtoList);
	}
	
	@Test
	void testForAccountIdNotFoundEx() {
		Mockito.when(orderHistoryRepository.findByAccountId(accountId)).thenReturn(null);
		assertThrows(AccountIdNotExistException.class, () -> {
			orderHistoryServiceImpl.getOrderHistoryDetails(accountId,sortBy);
			orderHistoryRepository.findByAccountId(accountId);
		});
	}
	
	@Test
	void ListIsEmptyEx() {
		Mockito.when(orderHistoryRepository.findAllByAccountId(accountId,pageable)).thenReturn(null);
		assertThrows(ListIsEmptyException.class, () -> {
			orderHistoryServiceImpl.getOrderHistoryDetails(accountId,sortBy);
			orderHistoryRepository.findAllByAccountId(accountId,pageable);
		});
	}
}
